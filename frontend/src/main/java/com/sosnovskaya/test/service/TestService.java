package com.sosnovskaya.test.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
@Configuration
@PropertySource(ignoreResourceNotFound = true, value = "classpath:application.properties")
public class TestService {

    @Value("${admin.username}")
    private String adminUsername;

    @Value("${admin.password}")
    private String adminPassword;

    @Value("${user.username}")
    private String userUsername;

    @Value("${user.password}")
    private String userPassword;

    @Value("${base.url}")
    private String baseUrl;

    private HttpClient httpClient;
    private ObjectMapper mapper;

    @Autowired
    public TestService(HttpClient httpClient) {
        mapper = new ObjectMapper();
        this.httpClient = httpClient;
    }

    public String callServerPublicEndpoint() throws IOException, InterruptedException {
        HttpResponse<String> mainResponse = httpClient
                .send(getGetHttpRequest("/public"),
                        HttpResponse.BodyHandlers.ofString());
        return mainResponse.body();
    }

    public String callServerUserEndpoint() throws IOException, InterruptedException {
        HttpResponse<String> mainResponse = httpClient
                .send(
                        getGetHttpRequestWithToken("/user",
                                getAccessToken(userUsername, userPassword)),
                        HttpResponse.BodyHandlers.ofString());
        return mainResponse.body();
    }

    public String callServerAdminEndpoint() throws IOException, InterruptedException {
        HttpResponse<String> mainResponse = httpClient
                .send(
                        getGetHttpRequestWithToken("/admin",
                                getAccessToken(adminUsername, adminPassword)),
                        HttpResponse.BodyHandlers.ofString());
        return mainResponse.body();
    }


    private HttpRequest getGetHttpRequestWithToken(String endpoint, String token) {
        return HttpRequest.newBuilder()
                .uri(URI.create(baseUrl + endpoint))
                .setHeader("Authorization", "Bearer " +  token)
                .GET()
                .build();
    }

    private HttpRequest getGetHttpRequest(String endpoint) {
        return HttpRequest.newBuilder()
                .uri(URI.create(baseUrl + endpoint))
                .setHeader("Content-Type", "application/json")
                .GET()
                .build();
    }

    private String getAccessToken(String username, String password) throws IOException, InterruptedException {
        String credentials = "client:password";
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));

        String oauthServerUrl =
                "http://localhost:8090/oauth/token?grant_type=password&username=" + username
                        + "&password=" + password;

        HttpResponse<String> mainResponse = httpClient
                .send(
                        HttpRequest.newBuilder()
                                .uri(URI.create(oauthServerUrl))
                                .setHeader("Authorization", "Basic " + encodedCredentials)
                                .POST(HttpRequest.BodyPublishers.noBody())
                                .build(),
                        HttpResponse.BodyHandlers.ofString());

        JsonNode actualObj = mapper.readTree(mainResponse.body());

        return actualObj.get("access_token").asText();
    }
}
