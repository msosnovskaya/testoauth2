package com.sosnovskaya.test.controller;

import com.sosnovskaya.test.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class TestController {

    @Autowired
    private TestService service;

    @GetMapping("/admin")
    @ResponseBody
    public String getAdminPage() throws IOException, InterruptedException {
        return service.callServerAdminEndpoint();
    }

    @GetMapping("/public")
    @ResponseBody
    public String getPublicPage() throws IOException, InterruptedException {
        return service.callServerPublicEndpoint();
    }

    @GetMapping("/user")
    @ResponseBody
    public String getUserPage() throws IOException, InterruptedException {
        return service.callServerUserEndpoint();
    }
}
