package com.sosnovskaya.oauth.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestOauthController {

    @GetMapping("/public")
    public String publicEndpoint() {
        return "Public Page";
    }

    @GetMapping("/user")
    public String privateEndpoint() {
        return "User Page";
    }

    @GetMapping("/admin")
    public String adminEndpoint() {
        return "Administrator Page";
    }
}