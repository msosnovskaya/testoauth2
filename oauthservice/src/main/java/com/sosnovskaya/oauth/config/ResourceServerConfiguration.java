package com.sosnovskaya.oauth.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .requestMatchers()
                .antMatchers("/public")
                .and()
                .authorizeRequests()
                .antMatchers("/public")
                .permitAll()
                .and()
                .requestMatchers()
                .antMatchers("/user")
                .and()
                .authorizeRequests()
                .antMatchers("/user")
                .hasRole("USER")
                .and()
                .requestMatchers()
                .antMatchers("/admin")
                .and()
                .authorizeRequests()
                .antMatchers("/admin")
                .hasRole("ADMIN");
    }
}
