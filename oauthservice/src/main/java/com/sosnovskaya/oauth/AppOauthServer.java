package com.sosnovskaya.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppOauthServer {

    public static void main(String[] args) {
        SpringApplication.run(AppOauthServer.class, args);
    }
}
